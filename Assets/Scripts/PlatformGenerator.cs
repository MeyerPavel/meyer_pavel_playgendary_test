﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformGenerator : MonoBehaviour
{

    #region Fields
    private const float DISTANCE_BETWEEN_MIN = 4f;
    private const float DISTANCE_BETWEEN_MAX = 9f;


    [SerializeField]
    private GameObject thePlatform;
    [SerializeField]
    private ObjectPooler[] theObjectPools;
    [SerializeField]
    private Transform generationPoint;


    private float[] platformWidths;
    private float distanceBetween;
    private int platformSelector;
    #endregion


    #region Unity lifecycle
    void Start ()
    {
        platformWidths = new float[ theObjectPools.Length ];

        for (int i = 0; i < theObjectPools.Length; i++)
        {
            platformWidths[i] = theObjectPools[i].GetPooledObject().GetComponent<BoxCollider2D>().size.x;
        }
	}
	

	void Update ()
    {
        if ( transform.position.x < generationPoint.position.x )
        {
            distanceBetween = Random.Range ( DISTANCE_BETWEEN_MIN , DISTANCE_BETWEEN_MAX );
            platformSelector = Random.Range( 0 , theObjectPools.Length );

            transform.position = new Vector3 ( transform.position.x + ( platformWidths[platformSelector] / 2 )  + distanceBetween, transform.position.y, transform.position.z);

            
            GameObject newPlatform = theObjectPools[platformSelector].GetPooledObject();

            newPlatform.transform.position = transform.position;
            newPlatform.transform.rotation = transform.rotation;

            newPlatform.SetActive( true );
        }
	}
    #endregion

}
