﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformDestroyer : MonoBehaviour
{

    #region Fields
    [SerializeField]
    private GameObject platformDestructionPoint;
    #endregion


    #region Unity lifecycle
    void Start ()
    {
        platformDestructionPoint = GameObject.Find( "PlatformDestructionPoint" );
	}
	

	void Update ()
    {
		if( transform.position.x < platformDestructionPoint.transform.position.x )
        {
            gameObject.SetActive( false );
        }
	}
    #endregion

}
