﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Stick : MonoBehaviour
{

    #region Fields
    private const float GROWTH_RATE = 0.3f;
    private const int MULTIPLIER_TORQUE = -60;


    private TouchScreenHandler touchScreenHandler;
    private Vector3 updateScaleY;
    new private Rigidbody2D rigidbody;
    private Collider2D previousCollider;
    private bool isLongStick = false;
    private bool isTouchRed = false;
    private bool isHorizontalStick = false; 
    private bool hasTouchDown = false;
    #endregion


    #region Properties
    public Collider2D PreviousCollider
    {
        get { return previousCollider; }
        set { previousCollider = value; }
    }
    
    public bool IsLongStick
    {
        get { return isLongStick; }
        set { isLongStick = value; }
    }
    
    public bool IsTouchRed
    {
        get { return isTouchRed; }
        set { isTouchRed = value; }
    }

    public bool IsHorizontalStick
    {
        get { return isHorizontalStick; }
        set { isHorizontalStick = value; }
    }
    #endregion
    

    #region Unity lifecycle
    void Awake()
    {
        rigidbody = GetComponent<Rigidbody2D>();
        updateScaleY = transform.localScale;
        touchScreenHandler = GameObject.FindGameObjectWithTag( "TouchHandler" ).GetComponent<TouchScreenHandler>();

    }


    void Update()
    {
        if ( touchScreenHandler.IsTouched )
        {
            updateScaleY.y += Time.deltaTime + GROWTH_RATE;
            transform.localScale = updateScaleY;
            hasTouchDown = true;
        }
        if ( !touchScreenHandler.IsTouched && hasTouchDown )
        {
            rigidbody.freezeRotation = false;
            rigidbody.AddTorque( MULTIPLIER_TORQUE * updateScaleY.y );
        }

        if ( rigidbody.rotation <= -85 )
        {
            rigidbody.rotation = -90;
            rigidbody.freezeRotation = true;
            IsHorizontalStick = true;
        }
    }
    #endregion


    #region Event handlers
    void OnTriggerEnter2D ( Collider2D collider )
    {
        if ( collider.tag == "RedBlock" )
        {
            if ( Mathf.Abs (collider.transform.position.x - transform.position.x) <= 0.5f && !isLongStick )
            {
                IsTouchRed = true;
            }
        }
        if ( collider.tag == "LongStickTrigger" )
        {
            if (rigidbody.rotation >= -60 )  // Если он коснулся под углом большим (-60) , то значит персонаж был в LastBlock
            {
                PreviousCollider = collider;
                collider.enabled = false;
                IsLongStick = false;
            }
            else
            {
                IsLongStick = true;
            }
        }
    }
    #endregion

}
