﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class MainMenu : MonoBehaviour
{

    #region Fields
    private const float WAIT_SECONDS = 0.01f;
    private const float CAMERA_APPROACH_SPEED = 0.1f;


    [SerializeField]
    private GameObject mainMenu;
    [SerializeField]
    private GameObject restartMenu;
    [SerializeField]
    private Camera mainCamera;
    [SerializeField]
    private GameObject touchHandler;
    [SerializeField]
    private GameObject soundEffects;
    [SerializeField]
    private Text audioControllerText;


    private bool isSoundOn = true;
    #endregion


    #region Public methods
    public void PlayGame ()
    {
       
        StartCoroutine( CameraTest() );
        mainMenu.SetActive( false );
        Time.timeScale = 1f;
    }


    public void AudioController ()
    {
        isSoundOn = !isSoundOn;
        if ( isSoundOn )
        {
            audioControllerText.text = "Sound on";
            AudioListener.volume = 1;
        }
        else
        {
            audioControllerText.text = "Sound off";
            AudioListener.volume = 0;
        }
    }
    #endregion


    #region Private methods
    private IEnumerator CameraTest()
    {
        while (true)
        {
            while ( mainCamera.orthographicSize > 15f )
            {
                yield return new WaitForSeconds( WAIT_SECONDS );
                mainCamera.orthographicSize -= CAMERA_APPROACH_SPEED;

            }
            yield return null;
        }
    }
    #endregion

}
