﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class TouchScreenHandler : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{

    #region Fields
    [SerializeField]
    private GameObject mainMenu;
    [SerializeField]
    private GameObject restartMenu;


    private bool isTouched = false;
    #endregion


    #region Properties
    public bool IsTouched
    {
        get
        {
            return isTouched;
        }

        set
        {
            isTouched = value;
        }
    }
    #endregion

    #region IPointerDownHandler
    public void OnPointerDown( PointerEventData eventData )
    {
        if( ( mainMenu.activeSelf && restartMenu.activeSelf ) == false)
        {
            IsTouched = true;
        }
        
    }
    #endregion


    #region IPointerUpHandler
    public void OnPointerUp( PointerEventData eventData )
    {
        IsTouched = false;
    }
    #endregion

}
