﻿using UnityEngine;

public class Character : MonoBehaviour
{

    #region Fields
    private const float MOVE_SPEED = 5.0f;


    [SerializeField]
    private GameManager gameManager;
    [SerializeField]
    private AudioSource scoreCountSound;
    [SerializeField]
    private AudioSource deathSound;


    private Stick stickResource;
    private Stick newStick;
    private ScoreManager scoreManager;
    private Vector3 colliderPosition;
    private Animator animator;
    private HingeJoint2D hingeJoint2D;
    private bool hasNewStickCreated = false;
    private bool isGrounded = false;
    private bool isStanding = false;
    private bool isTooLongStick = false;
    private int score = -1;
    #endregion

    #region Properties
    public int Score
    {
        get { return score; }
        set
        {
            score = value;
            if (value >= 0)
            {
                scoreManager.Refresh ();
                if ( value > 0 ) scoreCountSound.Play ();
            }
        }
    }

    public bool HasNewStickCreated
    {
        get{ return hasNewStickCreated; }

        set{ hasNewStickCreated = value; }
    }

    public bool IsTooLongStick
    {
        get { return isTooLongStick; }
        set { isTooLongStick = value; }
    }

    private CharState State
    {
        get { return (CharState) animator.GetInteger("State"); }
        set { animator.SetInteger("State",(int) value); }
    }
    #endregion


    #region Unity lifecycle
    void Awake ()
    {
        Time.timeScale = 0f;
        animator = GetComponent<Animator>();
        stickResource = Resources.Load<Stick>("StickPrefab");
        hingeJoint2D = stickResource.GetComponent<HingeJoint2D>();
        scoreManager = GameObject.FindGameObjectWithTag("ScoreManagerTag").GetComponent<ScoreManager>();
    }


    void Update ()
    {
        CheckGround();
        if ( !isGrounded )
        {
            isStanding = true;
        }
        if ( isStanding )
        {
            if ( newStick.IsHorizontalStick == true )
            {
                isStanding = false;
                IsTooLongStick = newStick.IsLongStick;
            }
        }
        if ( !isStanding )
        {
            Run ();
        }
        if ( newStick.IsTouchRed )
        {
            Score++;
            newStick.IsTouchRed = false;
        }
    }
    #endregion


    #region Private Methods
    private void Run()
    {
        
        Vector3 direction = new Vector3(1, 0, 0);
        transform.position = Vector3.MoveTowards(transform.position, transform.position + direction, MOVE_SPEED * Time.deltaTime);
        State = CharState.Run;
    }


    private void CheckGround()
    {
        Collider2D[] colliders = Physics2D.OverlapCircleAll(transform.position, 0.001f);

        isGrounded = colliders.Length > 1;
    }
    #endregion



    #region Event Handlers
    void OnTriggerEnter2D ( Collider2D collider )
    {
        if ( collider.tag == "LastBlock" && !IsTooLongStick )
        {
            if (newStick != null)
            {
                Destroy(newStick.gameObject);
                newStick.PreviousCollider.enabled = true;
            }
            colliderPosition = collider.transform.position;
            colliderPosition.y += 0.6f;
            hingeJoint2D.connectedAnchor = colliderPosition;
            newStick = Instantiate(stickResource, colliderPosition, transform.rotation) as Stick;
            HasNewStickCreated = true;
            isStanding = true;
            State = CharState.Idle;
            Score++;
        }


        if ( collider.tag == "LongStickTrigger" && IsTooLongStick )
        {
            if ( newStick.PreviousCollider != null )
            {
                newStick.PreviousCollider.enabled = true;
            }
            Destroy( newStick.gameObject );
        }
    }


    void OnCollisionEnter2D ( Collision2D other )
    {
        if ( other.gameObject.tag == "killbox" )
        {
            deathSound.Play();
            gameManager.RestartGameMenu();
            Time.timeScale = 0;
        }
    }
    #endregion

}

public enum CharState
{
    Idle,
    Run
}