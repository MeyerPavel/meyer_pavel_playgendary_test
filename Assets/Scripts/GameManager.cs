﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{

    #region Fields
    [SerializeField]
    private Transform platformGenerator;
    [SerializeField]
    private Character character;
    [SerializeField]
    private GameObject restartMenu;


    private PlatformDestroyer[] platformList;
    private Vector3 platformStartPoint;
    private Vector3 playerStartPoint;
    #endregion


    #region Unity lifecycle
    void Start ()
    {
        platformStartPoint = platformGenerator.position;
        playerStartPoint = character.transform.position;
	}
    #endregion


    #region Public methods
    public void RestartGameMenu()
    {
        restartMenu.SetActive ( true );
    }


    public void RestartGame ()
    {
        Time.timeScale = 1f;
        StartCoroutine ( RestartGameCo() );
    }

    
    public IEnumerator RestartGameCo ()
    {
        character.gameObject.SetActive ( false );
        character.Score = -1;
        character.IsTooLongStick = false;
        yield return new WaitForSeconds(1);
        platformList = FindObjectsOfType<PlatformDestroyer>();
        for(int i = 0; i < platformList.Length; i++)
        {
            platformList[i].gameObject.SetActive ( false );
        }

        character.transform.position = playerStartPoint;
        platformGenerator.position = platformStartPoint;
        character.gameObject.SetActive ( true );
        restartMenu.SetActive ( false );
    }
    #endregion

}
