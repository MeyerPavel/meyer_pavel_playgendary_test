﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{

    #region Fields
    [SerializeField]
    private Text scoreText;
    [SerializeField]
    private Text hiScoreText;
    [SerializeField]
    private Text pauseScoreText;
    [SerializeField]
    private Text pauseHiScoreText;


    private Character character;
    private float hiScore = 0f;
    #endregion


    #region Unity lifecycle
    void Awake()
    {
        character = GameObject.FindGameObjectWithTag("Player").GetComponent<Character>();
    }
    #endregion


    #region Public methods
    public void Refresh()
    {
        if(character.Score > hiScore)
        {
            hiScore = character.Score;
        }
        scoreText.text = "Score: " + character.Score;
        hiScoreText.text = "High Score: " + hiScore;
        pauseScoreText.text = scoreText.text;
        pauseHiScoreText.text = hiScoreText.text;
    }
    #endregion

}
