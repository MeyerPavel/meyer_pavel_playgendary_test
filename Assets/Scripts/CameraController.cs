﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{

    #region Fields
    [SerializeField]
    private Character character;


    private Vector3 lastPlayerPosition;
    private float distanceToMove;
    #endregion


    #region Unity lifecycle
    void Start ()
    {
        character = GameObject.FindGameObjectWithTag( "Player" ).GetComponent<Character>();
        lastPlayerPosition = character.transform.position;
    }
	
	
	void Update ()
    {
        distanceToMove = character.transform.position.x - lastPlayerPosition.x;

        transform.position = new Vector3 ( transform.position.x + distanceToMove , transform.position.y , transform.position.z );

        lastPlayerPosition = character.transform.position;
	}
    #endregion

}
