﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPooler : MonoBehaviour
{

    #region Fields
    [SerializeField]
    private GameObject pooledObject;
    [SerializeField]
    private int pooledAmount;


    List<GameObject> pooledObjectsList;
    #endregion


    #region Unity lifecycle
    void Awake ()
    {
        pooledObjectsList = new List<GameObject>();

        for (int i = 0; i < pooledAmount; i++)
        {
            GameObject obj = Instantiate( pooledObject );
            obj.SetActive( false );
            pooledObjectsList.Add( obj );
        }
	}
    #endregion


    #region Public Methods
    public GameObject GetPooledObject()
    {
        for (int i = 0; i < pooledObjectsList.Count; i++)
        {
            if (!pooledObjectsList[i].activeInHierarchy)
            {
                return pooledObjectsList[i];
            }
        }

        GameObject obj = Instantiate(pooledObject);
        obj.SetActive(false);
        pooledObjectsList.Add (obj);
        return obj;
    }
    #endregion

}
